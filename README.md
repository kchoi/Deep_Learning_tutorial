# Deep Learning tutorial

This repository contains materials for the Deep Learning tutorial of the Intuidoc team.

## Connecting to IGRIDA

### Prerequisite

You will need a working `ssh` with private and public key pairs.

#### For Windows users:

Install git for windows https://git-scm.com/downloads.
This will install `Git Bash` with the three programs you will need: `ssh`, `ssh-keygen` and `ssh-copy-id`.

Next, launch `git bash` and create your private/public keys with `ssh-keygen` and copy them to the `sabre.irisa.fr` server with `ssh-copy-id`:

```bash
ssh-keygen -t rsa
ssh-copy-id -i ~/.ssh/id_rsa.pub $USER@sabre
```

Maintenant, vous pouvez vous connecter sur le front-end de la grille en utilisant la commande:

```bash
ssh $USER@igrida-oar-frontend
```

## Launching IGRIDA jobs for the tutorial

In order to launch jobs on IGRIDA, you need to submit jobs using the `oarsub` command:

```bash
# create the job (add -t timesharing if you do this tutorial with lots of people in parallel)
# the option gpu_device=1 ask the grid scheduler for a node with 1 gpu
# walltime is the maximum duration of the job, here we ask for 40 hours.
oarsub -I -l gpu_device=1,walltime=40:00:0
```

This will launch an interactive job, on which you will be connected immediately.
If you loose your connection or close your terminal, the job will be stopped immediately.
That is why I prefer to launch a job using a staller script, on which you can connect and disconnect freely.

```bash
# first clone this project on your /udd/$USER space
cd $HOME  # or wherever you want to clone this repo
git clone https://gitlab.inria.fr/kchoi/Deep_Learning_tutorial.git
cd Deep_Learning_tutorial
# create the job (add -t timesharing if you do this tutorial with lots of people in parallel)
oarsub -S -l gpu_device=1,walltime=01:30:0 scripts/run_igrida_staller.sh
# you can check the status of your jobs with the oarstat command
oarstat -u
# connect to it using the Job id
oarsub -C JOB_ID
```

You can find the script  [`scripts/run_igrida_staller.sh`](scripts/run_igrida_staller.sh).

More info at http://igrida.gforge.inria.fr/

## Clone the project

If you have not done it already, you can now clone the project on igrida with git:

```bash
cd where_ever_you_want
git clone https://gitlab.inria.fr/kchoi/Deep_Learning_tutorial.git
```

## Install miniconda

[Miniconda](https://docs.conda.io/en/latest/miniconda.html#) is the prefered way of installing a python environment for deep learning.

```bash
# get the install script
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
# launch the script
# by default miniconda will install in your $HOME/miniconda3
# you can choose another directory, but do not install on /temp_dd/igrida-fs1/$USER !!
bash Miniconda3-latest-Linux-x86_64.sh
# if not already done, activate the miniconda environment
source $HOME/miniconda3/bin/activate
# make sure that your python is really the miniconda one
which python
> /home/$USER/miniconda3/bin/python
# install keras, jupyter and matplotlib
conda install keras-gpu jupyter matplotlib  # use keras if no gpu
```

## Load the jupyter notebook

Next, run the jupyter notebook from which we will be doing the tutorial:

```bash
jupyter notebook --ip=* --no-browser
# output

[W 20:14:43.648 NotebookApp] WARNING: The notebook server is listening on all IP addresses and not using encryption. This is not recommended.
[I 20:14:43.681 NotebookApp] Serving notebooks from local directory: /udd/user
[I 20:14:43.681 NotebookApp] 0 active kernels
[I 20:14:43.682 NotebookApp] The Jupyter Notebook is running at:
[I 20:14:43.682 NotebookApp] http://[all ip addresses on your system]:8888/?token=ad046861594b9231e8f9c82860e7b25ca13d7637aee7aee5
[I 20:14:43.682 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[C 20:14:43.684 NotebookApp]

    Copy/paste this URL into your browser when you connect for the first time,
    to login with a token:
        http://localhost:8888/?token=ad046861594b92...
```

Follow the output of the command and connect to the link `http://igrida-abacus:8888/?token=ad046861594b923...`.
Remember to replace `localhost` to the server you actually are running a job on.

Once you have loaded the jupyter notebook on your browser, you can start the tutorial by the opening the first [notebook](1. Introduction to Deep Learning.ipynb).

That's all for now, happy tutorial!