#!/bin/bash
set -xv
set -e

echo
echo OAR_WORKDIR : $OAR_WORKDIR
echo
echo "cat \$OAR_NODE_FILE :"
cat $OAR_NODE_FILE
echo

echo "
##########################################################################
# Where will your run take place ?
#
# * It is NOT recommanded to run in $HOME/... (especially to write), 
#   but rather in /temp_dd/igrida-fs1/...
#   Writing directly somewhere in $HOME/... will necessarily cause NFS problems at some time.
#   Please respect this policy.
#
# * The program to run may be somewhere in your $HOME/... however
#
##########################################################################
"

#source /udd/kchoi/prog/utils/set_env.sh

echo "=============== RUN ==============="

echo "Running ..."

sleep 54000

echo
echo OK

